SELECT
	c.category_name,
	SUM(i.item_price) AS total_price
FROM
	item i
INNER JOIN
	item_category c
ON
	i.category_id = c.category_id
GROUP BY
	c.category_name
-- ORDER BYを最後に持ってこないとエラーになる(グループ化や集計を全て終えてから並び替え)
ORDER BY
	total_price DESC
