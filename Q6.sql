SELECT
    i.item_name,
    i.item_price
FROM
    item i
WHERE
    i.item_price >= 1000
